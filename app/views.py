import json

from aiohttp import web

BYTES = b'Response in bytes'
DICTIONARY = {
    "message": "Response in JSON", 
    "success": True
    }
DICTIONARIES = {}

async def blueprint_enrich(request):
    """ Blueprint enrichment """
    return web.Response(body=BYTES, status=200)

async def blueprint_publish(request):
    """ Blueprint publishing """
    return web.Response(body=BYTES, status=200)

async def dictionary(request):
    """ Data dictionary """
    try:
        body = await request.json()
        DICTIONARIES[body["name"]] = body
    except ValueError:
        print("No JSON sent! Leave it because we used that endpoint during "
              "the availability and we won't break the integration tests")
    return web.json_response(data=DICTIONARY, status=200)

async def dictionary_get(request):
    """ Data dictionary get """
    name = request.match_info["name"]
    try:
        return web.json_response(data=DICTIONARIES[name], status=200)
    except KeyError:
        return web.Response(status=404)
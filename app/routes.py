from views import blueprint_enrich, blueprint_publish, dictionary, dictionary_get

def setup_routes(app):
    app.router.add_post('/api/v1/blueprint-model/enrich', blueprint_enrich)
    app.router.add_post('/api/v1/blueprint-model/publish', blueprint_publish)
    app.router.add_post('/api/v1/dictionary', dictionary)
    app.router.add_get('/api/v1/dictionary/{name}', dictionary_get)